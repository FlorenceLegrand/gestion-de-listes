//creer une api rest avec expressJS
// a demarrer avec npm start ou nodemon sevrer dans le dossier backend
//demarrer avec localhost:4201

const express = require('express');
const app = express();
const bodyParser = require('body-parser');

let data = require ('./liste');

let initialListe = data.liste;
let addedOptions = [];

const getAllOptions = () => {
    return [...addedOptions, ...initialListe]
}


app.use(bodyParser.json());

app.use((req,res,next)=>{
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

const api = express.Router();

api.get('/liste', (req,res)=> {
    res.json(getAllOptions());
});

api.post('/liste',(req,res)=>{
    const liste = req.body;
    console.log(liste);
    addedOptions = [liste,...addedOptions];
    console.log('total number of options :',getAllOptions().length)
    res.json(liste);
;});

api.get('/search/:key', (req,res)=>{
    const key = req.params.key.toLowerCase().trim();
    let liste = getAllOptions().filter(j => (j.key.toLowerCase().includes(key)));
    res.json({success:true,liste:liste});
});

app.use('/api',api); 

const port = 4201;

app.listen(port, () => {
    console.log(`listening on port ${port}`);
});
