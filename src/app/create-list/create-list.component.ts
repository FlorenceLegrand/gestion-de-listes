//ne pas oublier les import dans app.module.ts
//FormsModule,ReactiveFormsModule
import { Component, OnInit } from '@angular/core';
import { FormBuilder , FormGroup} from '@angular/forms';
import { ListeService } from '../services/liste.service';


@Component({
  selector: 'app-create-list',
  templateUrl: './create-list.component.html',
  styleUrls: ['./create-list.component.css']
})
export class CreateListComponent implements OnInit {

  formCreate : FormGroup;
  constructor(private formBuilder:FormBuilder,
              private listeService:ListeService
            ) { }

  ngOnInit() {
    this.formCreate = this.formBuilder.group({
      id:0,
      key:'',
      label:''
    });
  }
createList(dataList){
  this.listeService.addList(dataList).subscribe();
  //console.log(dataList);

  this.formCreate.reset();
}
}
