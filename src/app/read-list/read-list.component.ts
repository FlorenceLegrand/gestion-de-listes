import { Component, OnInit } from '@angular/core';
import { ListeService } from '../services/liste.service';


@Component({
  selector: 'app-read-list',
  templateUrl: './read-list.component.html',
  styleUrls: ['./read-list.component.css']
})
export class ReadListComponent implements OnInit {

  liste =[];
  constructor(private listService : ListeService) { }

  ngOnInit() {
    this.listService.getList()
                    .subscribe( data=>{
                                      //console.log(data);
                                      this.liste = data;
                              });
    //pour que le contenu du formulaire soit visible dans la liste
    //le subject fait le lien avec les donnees du service
    this.listService.listeSubject.subscribe(data=>{
                                            //on met les nouvelles data au debut du tableau listes
                                              this.liste = [...this.liste,data];
                                            })
  }

}
