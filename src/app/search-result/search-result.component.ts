import { Component, OnInit } from '@angular/core';
import { ListeService } from '../services/liste.service';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {
  liste = [];
  constructor(private listeService:ListeService) { }

  ngOnInit() {
    this.listeService.searchResultSubject.subscribe(
      data => this.handleSearchResult(data)//on recupere les donnees data dans handlesearchsubject
    )
  }
  handleSearchResult(data){
    //on utilise data.liste car api renvoie un objet {success:true,liste:liste} a searchList du service
    this.liste = data.liste;//on rempli jobs avec les jobs de data
  }

}
