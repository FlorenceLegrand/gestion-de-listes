import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ReadListComponent } from './read-list/read-list.component';
import { CreateListComponent } from './create-list/create-list.component';
import { HomeComponent } from './home/home.component';
import { ListeService } from './services/liste.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
//pour formulaire
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SearchListComponent } from './search-list/search-list.component';
import { SearchResultComponent } from './search-result/search-result.component';

const routes = [
  {path :'', component: HomeComponent},
  {path :'liste/add', component: CreateListComponent},
  {path : 'liste', component : ReadListComponent },//localhost:4200/jobs  liste des jobs
] 
@NgModule({
  declarations: [
    AppComponent,
    ReadListComponent,
    CreateListComponent,
    HomeComponent,
    SearchListComponent,
    SearchResultComponent
  ],
  imports: [
    BrowserModule,
    //pour recuperer le contenu du json
    HttpClientModule,
    HttpModule,
    //pour formulaire
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ListeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
