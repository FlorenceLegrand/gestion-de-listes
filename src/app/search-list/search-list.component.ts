import { Component, OnInit } from '@angular/core';
import { ListeService } from '../services/liste.service';

@Component({
  selector: 'app-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.css']
})
export class SearchListComponent implements OnInit {
  liste=[];
  // AVOIR
//UTILISER POUR LISTE DEROULANTE
  listekey = [
    {id: 1, key: 'OPT1', value: 'OPT1', label: 'option1'},
    {id: 2, key: 'OPT2', value: 'OPT2', label: 'option2'},
    {id: 3, key: 'OPT3', value: 'OPT3', label: 'option3'},
    {id: 4, key: 'OPT4', value: 'OPT4', label: 'option4'}
  ];
  constructor(private listeService:ListeService) { }

  ngOnInit() {
  }
  resultForm(searchData){
    this.listeService.searchList(searchData)
                    .subscribe(
                      data => this.liste = data,//on met les data dans variable liste
                    );
  }

}
