    //ne pas mettre de console.log dans un service

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import { Subject } from 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ListeService {

  initialListe=[];
  liste = [];
  listeSubject = new Subject();
  searchResultSubject = new Subject();
BASE_URL = 'http://localhost:4201/';

  constructor(private http:Http) { }

  getList() {
        return this.http.get(this.BASE_URL + 'api/liste')
                      .map(res => res.json())//on met dans res la reponse du get en json
                      
  }

  
  addList(dataListe){
    //RETOURNE 000!!!!!!!
    dataListe.id = 0;//trouver comment recuperer id
    //dataListe.id = Date.now();
    //this.liste = [...this.liste,dataListe];
    //console.log(this.liste);
    //return this.listeSubject.next(dataListe);
    return this.http.post(this.BASE_URL + 'api/liste',dataListe)
                    .map ( res => {
                      console.log(res);
                      this.listeSubject.next(dataListe);
                    })
  }

  searchList(critere){
    return this.http.get(`${this.BASE_URL}api/search/${critere.key}`)
                    .map(res =>res.json())
                    //partie pour affichage de la recherche 
                    //on utilise do pour recuperer le resultat qui sera recuperer par celui qui s'abonnera au subjet
                    //utiliser par searchresultcomponent
                    .do(res => this.searchResultSubject.next(res));
  }
}
